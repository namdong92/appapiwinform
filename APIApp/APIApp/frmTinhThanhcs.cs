﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APIApp
{
    public partial class frmTinhThanhcs : Form
    {
        public frmTinhThanhcs()
        {
            InitializeComponent();
        }

        private async void frmTinhThanhcs_Load(object sender, EventArgs e)
        {
            dgvTinhThanh.Columns.Add("Id", "Mã Tỉnh Thành");
            dgvTinhThanh.Columns.Add("Name", "Tên Tỉnh Thành");
            dgvTinhThanh.Columns.Add("Parent", "Cấp Trên");
            dgvTinhThanh.Columns.Add("IsShow", "Ẩn Hiện");
            dgvTinhThanh.Columns.Add("Note", "Ghi Chú");
            dgvTinhThanh.Columns.Add("Alias", "Tên Không Dấu");

            dgvTinhThanh.Columns["Id"].DataPropertyName = "Id";
            dgvTinhThanh.Columns["Name"].DataPropertyName = "Name";
            dgvTinhThanh.Columns["Parent"].DataPropertyName = "Parent";
            dgvTinhThanh.Columns["IsShow"].DataPropertyName = "IsShow";
            dgvTinhThanh.Columns["Note"].DataPropertyName = "Note";
            dgvTinhThanh.Columns["Alias"].DataPropertyName = "Alias";
            
            try
            {
                using (var client = new HttpClient()) {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HttpResponseMessage response = 
                        await client.GetAsync("https://nguyenvando.net/api/getLocation/");
                    response.EnsureSuccessStatusCode();
                    
                    //MessageBox.Show(responseBody);
                    using (HttpContent content = response.Content) {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        List<TinhThanh> DSTinhThanh = 
                            JsonConvert.DeserializeObject<List<TinhThanh>>(responseBody);
                        dgvTinhThanh.DataSource = DSTinhThanh;
                    }
                    

                }
            }
            catch (Exception)
            {

                throw;
            }


        }
    }
}
