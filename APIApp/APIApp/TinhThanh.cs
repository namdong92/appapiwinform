﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIApp
{
    class TinhThanh
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public string Parent { set; get; }
        public string isShow { set; get; }
        public string Note { set; get; }
        public string Alias { set; get; }
    }
}
