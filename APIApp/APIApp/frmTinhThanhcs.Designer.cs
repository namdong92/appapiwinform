﻿namespace APIApp
{
    partial class frmTinhThanhcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTinhThanh = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTinhThanh)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTinhThanh
            // 
            this.dgvTinhThanh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTinhThanh.Location = new System.Drawing.Point(12, 144);
            this.dgvTinhThanh.Name = "dgvTinhThanh";
            this.dgvTinhThanh.Size = new System.Drawing.Size(776, 294);
            this.dgvTinhThanh.TabIndex = 0;
            // 
            // frmTinhThanhcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgvTinhThanh);
            this.Name = "frmTinhThanhcs";
            this.Text = "frmTinhThanhcs";
            this.Load += new System.EventHandler(this.frmTinhThanhcs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTinhThanh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTinhThanh;
    }
}