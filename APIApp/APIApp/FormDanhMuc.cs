﻿using APIApp.ModuleDanhMuc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APIApp
{
    public partial class FormDanhMuc : Form
    {
        public FormDanhMuc()
        {
            InitializeComponent();
        }

        private async void FormDanhMuc_Load(object sender, EventArgs e)
        {

            //    public string MaDanhMuc { set; get; }
            //public string TenDanhMuc { set; get; }
            //public string TenKhongDau { set; get; }
            //public string UrlHinh { set; get; }
            //public string CapCha { set; get; }
            //public int TongTin { set; get; }
            //public string Link { set; get; }
            dgvDanhMuc.Columns.Add("MaDanhMuc", "#");
            dgvDanhMuc.Columns.Add("TenDanhMuc", "Tên");
            dgvDanhMuc.Columns.Add("TenKhongDau", "Tên Không Dấu");
            dgvDanhMuc.Columns.Add("Link", "Link");
            dgvDanhMuc.Columns.Add("CapCha", "Danh Mục Cha");
            dgvDanhMuc.Columns.Add("TongTin", "Tổng Tin");
            dgvDanhMuc.Columns["MaDanhMuc"].DataPropertyName = "MaDanhMuc";
            dgvDanhMuc.Columns["TenDanhMuc"].DataPropertyName = "TenDanhMuc";
            dgvDanhMuc.Columns["TenKhongDau"].DataPropertyName = "TenKhongDau";
            dgvDanhMuc.Columns["Link"].DataPropertyName = "Link";
            dgvDanhMuc.Columns["CapCha"].DataPropertyName = "CapCha";
            dgvDanhMuc.Columns["TongTin"].DataPropertyName = "TongTin";


            try
            {
                DanhMucService DMS = new DanhMucService();
                List<DanhMuc> a = await DMS.GetAll();
                dgvDanhMuc.DataSource = a;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
