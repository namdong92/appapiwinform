﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIApp.ModuleDanhMuc
{
    class DanhMuc
    {
        public string MaDanhMuc { set; get; }
        public string TenDanhMuc { set; get; }
        public string TenKhongDau { set; get; }
        public string UrlHinh { set; get; }
        public string CapCha { set; get; }
        public int TongTin { set; get; }
        public string Link { set; get; }
    }
}
