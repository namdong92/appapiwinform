﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace APIApp.ModuleDanhMuc
{
    class DanhMucService
    {
        public async Task<List<DanhMuc>> GetAll() {

            try
            {
                using (var client = new HttpClient())
                {
                   HttpResponseMessage response =
                        await client
                        .GetAsync("https://sangquan.net/api/danhsachdanhmuc/");
                    response.EnsureSuccessStatusCode();

                    //MessageBox.Show(responseBody);
                     
                        string responseBody = await response
                        .Content
                        .ReadAsStringAsync();
                        List<DanhMuc> DSDanhMuc =
                            JsonConvert
                            .DeserializeObject<List<DanhMuc>>(responseBody);
                    return DSDanhMuc;
                }
            }
            catch (Exception)
            {

                throw;
            }
             
        }

    }
}
